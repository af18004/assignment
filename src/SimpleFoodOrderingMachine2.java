import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine2 {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton udonButton;
    private JButton sobaButton;
    private JButton sushiButton;
    private JButton tomatoButton;
    private JButton ramenButton;
    private JTextPane receivedInfo;
    private JButton checkOutButton;
    private JLabel totalLabel;
    private JLabel orderedItem;
    int sum = 0;

    public SimpleFoodOrderingMachine2() {
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("M.png")));
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("M.png")));
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("M.png")));
        sobaButton.setIcon(new ImageIcon(this.getClass().getResource("M.png")));
        sushiButton.setIcon(new ImageIcon(this.getClass().getResource("M.png")));
        tomatoButton.setIcon(new ImageIcon(this.getClass().getResource("M.png")));
        totalLabel.setText("Total        " + sum + "　yen"); // 初期値


        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",100);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",90);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",200);
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",300);
            }
        });
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",350);
            }
        });
        tomatoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tomato",2);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,"would you like to Check Out?",
                        "Checkout Confirmation",JOptionPane.YES_NO_OPTION);
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is " + sum +  " yen.");
                    receivedInfo.setText("");
                    sum = 0;
                    totalLabel.setText("Total        " + sum + " yen"); //sumを初期値にリセット
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine2");
        frame.setContentPane(new SimpleFoodOrderingMachine2().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to " + food + " ?","Order Confirmation",JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            JOptionPane.showMessageDialog(null,"Thank you for ordering " + food +  " ! It will be served as soon as possible.");
            receivedInfo.setText(receivedInfo.getText() + food + "　　" +price + "　yen" +"\n");
            sum += price;
            totalLabel.setText("Total        " + sum + " yen");
        }

    }
}
